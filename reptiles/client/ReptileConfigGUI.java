package com.reptiles.client;

import com.reptiles.common.ConfigHandler;
import com.reptiles.common.Reptiles;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;

public class ReptileConfigGUI extends GuiConfig {
    public ReptileConfigGUI(GuiScreen parentScreen) {
        super(parentScreen,
                new ConfigElement(ConfigHandler.getConfig().getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
                Reptiles.MODID, true, false, GuiConfig.getAbridgedConfigPath(ConfigHandler.getConfig().toString()));
    }
}
