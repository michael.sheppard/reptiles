/*
 * EntityCroc.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.reptiles.common;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityAgeable;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.*;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemStack;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class EntityCrocBase extends EntityTameable {

    private final float scaleFactor;
    private final int maxHealth = 20;
    private static final DataParameter<Float> health = EntityDataManager.createKey(EntityCrocBase.class, DataSerializers.FLOAT);

    public EntityCrocBase(World world) {
        super(world);
        setSize(2.0F, 0.3F);
        setHealth(20);

        if (ConfigHandler.useRandomScaling()) {
            float scale = rand.nextFloat();
            scaleFactor = scale < 0.6F ? 1.0F : scale;
        } else {
            scaleFactor = 1.0F;
        }
    }

    @Override
    protected void initEntityAI() {
        tasks.addTask(0, new EntityAISwimming(this));
        tasks.addTask(1, new EntityAILeapAtTarget(this, 0.5F));
        tasks.addTask(2, new EntityAIAttackMelee(this, 1.0D, true));
        tasks.addTask(3, new EntityAIWander(this, 1.0));
        tasks.addTask(4, new EntityAIWatchClosest(this, EntityPlayer.class, 8.0F));
        tasks.addTask(5, new EntityAILookIdle(this));
        tasks.addTask(7, new EntityAIMate(this, 1.0));

        if (ConfigHandler.getFollowOwner()) {
            tasks.addTask(6, new EntityAIFollowOwner(this, 1.0, 10.0F, 2.0F));
            targetTasks.addTask(2, new EntityAIOwnerHurtTarget(this));
        }

        targetTasks.addTask(1, new EntityAIHurtByTarget(this, false));
        targetTasks.addTask(2, new EntityAITargetNonTamed<>(this, EntityPlayer.class, false, entity -> true));
        targetTasks.addTask(3, new EntityAITargetNonTamed<>(this, EntityCow.class, false, entity -> true));
        targetTasks.addTask(4, new EntityAITargetNonTamed<>(this, EntitySheep.class, false, entity -> true));
        targetTasks.addTask(4, new EntityAITargetNonTamed<>(this, EntityPig.class, false, entity -> true));
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    public void onUpdate() {
        // kill in cold biomes
        BlockPos bp = new BlockPos(posX, posY, posZ);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) < 0.75) {
            setAIMoveSpeed(0.5f);
        }
        if (biome.getTemperature(bp) < 0.50) {
            attackEntityFrom(DamageSource.STARVE, 2.0f);
        }
		super.onUpdate();
	}

    @Override
    protected boolean canDespawn() {
        return false;
    }

    @Override
    public float getEyeHeight() {
        return 0.25f;
    }

    @Override
    public boolean getCanSpawnHere() {
        BlockPos bp = new BlockPos(posX, posY, posZ);
        Biome biome = world.getBiome(bp);
        return (biome.getTemperature(bp) > 0.75) && super.getCanSpawnHere();

    }

    @Override
    public void setAttackTarget(@Nullable EntityLivingBase entitylivingbaseIn) {
        super.setAttackTarget(entitylivingbaseIn);
    }

    @Override
    protected void applyEntityAttributes() {
        super.applyEntityAttributes();
        if (isTamed()) {
            getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(20.0D);
        } else {
            getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        }
        getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25);
        getAttributeMap().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(3.0D);
    }

    @Override
    protected void entityInit() {
        super.entityInit();
        dataManager.register(health, getHealth());
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return ReptileSounds.croc_growl;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return ReptileSounds.croc_growl;
    }

    @Override
    protected void playStepSound(BlockPos blockPos, Block block) {
        playSound(SoundEvents.ENTITY_COW_STEP, 0.15F, 1.0F);
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    protected Item getDropItem() {
        return Reptiles.CROC_LEATHER;
    }

    @Override
    protected void dropFewItems(boolean flag, int add) {
        int count = rand.nextInt(3) + rand.nextInt(1 + add);
        dropItem(Reptiles.CROC_LEATHER, count);

        count = rand.nextInt(3) + 1 + rand.nextInt(1 + add);
        if (isBurning()) {
            dropItem(Reptiles.CROC_MEAT_COOKED, count);
        } else {
            dropItem(Reptiles.CROC_MEAT_RAW, count);
        }
    }

    @Override
    protected int getExperiencePoints(EntityPlayer par1EntityPlayer) {
        return 1 + world.rand.nextInt(4);
    }

    // favorite food for breeding
    private boolean isFavoriteFood(ItemStack itemstack) {
        return (itemstack != null && (itemstack.getItem() == Items.COOKED_PORKCHOP));
    }

    @Override
    public boolean isBreedingItem(ItemStack itemStack) {
        return itemStack != null && (itemStack.getItem() instanceof ItemFood && isFavoriteFood(itemStack));
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(health, getHealth());
    }

    @Override
    public boolean processInteract(EntityPlayer entityplayer, @Nonnull EnumHand enumHand) {
        if (!ConfigHandler.getAllowTameCrocs()) {
            return false;
        }
        ItemStack itemstack = entityplayer.getHeldItem(enumHand);

        if (isTamed()) {
            if (!itemstack.isEmpty()) {
                if (itemstack.getItem() instanceof ItemFood) {
                    ItemFood itemfood = (ItemFood) itemstack.getItem();
                    if (isFavoriteFood(itemstack) && dataManager.get(health) < maxHealth) {
                        if (!entityplayer.capabilities.isCreativeMode) {
                            itemstack.shrink(1);
                        }

                        heal((float) itemfood.getHealAmount(itemstack));
                        return true;
                    }
                }
            }

            if (isOwner(entityplayer) && !world.isRemote && !isBreedingItem(itemstack)) {
//                aiSit.setSitting(!isSitting());
                isJumping = false;
                navigator.clearPath();
                setAttackTarget(null);
            }
        } else if (itemstack.getItem() == Items.PORKCHOP) { // raw porkchop to tame
            if (!entityplayer.capabilities.isCreativeMode) {
                itemstack.shrink(1);
            }

            if (!world.isRemote) {
                if (rand.nextInt(3) == 0) {
                    setTamed(true);
                    navigator.clearPath();
                    setAttackTarget(null);
//                    aiSit.setSitting(true);
                    setHealth(maxHealth);
                    setOwnerId(entityplayer.getUniqueID());
                    playTameEffect(true);
                    world.setEntityState(this, (byte) 7);
                } else {
                    playTameEffect(false);
                    world.setEntityState(this, (byte) 6);
                }
            }
            return true;
        }

        return super.processInteract(entityplayer, enumHand);
    }

    @Override
    public boolean canMateWith(@Nonnull EntityAnimal otherAnimal) {
        if (otherAnimal == this) {
            return false;
        } else if (!isTamed()) {
            return false;
        } else if (!(otherAnimal instanceof EntityCrocBase)) {
            return false;
        } else {
            EntityCrocBase v = (EntityCrocBase) otherAnimal;
            return v.isTamed() && (!v.isSitting() && (isInLove() && v.isInLove()));
        }
    }

    // This MUST be overridden in the derived class
    public EntityAnimal spawnBabyAnimal(EntityAgeable entityageable) {
        Reptiles.instance.error("[ERROR] Do NOT call this base class method directly!");
        return null;
    }

    @Override
    public EntityAgeable createChild(@Nonnull EntityAgeable entityAgeable) {
        return this.spawnBabyAnimal(entityAgeable);
    }

    @Override
    public void setTamed(boolean tamed) {
        super.setTamed(tamed);

        if (tamed) {
            getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(maxHealth);
        } else {
            getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        }
    }

    @Override
    public boolean canBeLeashedTo(EntityPlayer player) {
        return super.canBeLeashedTo(player);
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        return entity.attackEntityFrom(DamageSource.causeMobDamage(this), 4);
    }

    // this makes the crocs move fast in water. Surprise!!
    @Override
    protected float getWaterSlowDown() {
        return 1.0F;
    }

}
